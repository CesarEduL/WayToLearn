import 'package:flutter_dotenv/flutter_dotenv.dart';

class Env {
  static String get apiKey => dotenv.env['OPENAI_API_KEY'] ?? '';

  static Future<void> loadEnv() async {
    await dotenv.load(fileName: ".env");
  }
}
