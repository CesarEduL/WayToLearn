import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter_tts/flutter_tts.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:dart_openai/dart_openai.dart';
import '/services/env.dart';

class StoryDetailPage extends StatefulWidget {
  final String cuentoId;

  const StoryDetailPage({Key? key, required this.cuentoId}) : super(key: key);

  @override
  _StoryDetailPageState createState() => _StoryDetailPageState();
}

class _StoryDetailPageState extends State<StoryDetailPage> {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final DatabaseReference _databaseRef = FirebaseDatabase.instance.ref();
  Map<String, dynamic>? cuentoData;
  int initialScore = 0;
  int totalScore = 0;
  FlutterTts flutterTts = FlutterTts();
  bool isPlaying = false;

  @override
  void initState() {
    super.initState();
    _fetchCuentoData();
    flutterTts.setCompletionHandler(() {
      setState(() {
        isPlaying = false;
      });
    });
    OpenAI.apiKey = Env.apiKey; // Configura la API key de OpenAI
  }

  void _fetchCuentoData() async {
    final User? user = _auth.currentUser;

    if (user != null) {
      final DatabaseEvent event =
          await _databaseRef.child('users/${user.uid}/totalScore').once();
      final DataSnapshot snapshot = event.snapshot;

      if (snapshot.value != null) {
        setState(() {
          totalScore = snapshot.value as int;
        });
      }
    }

    final DatabaseEvent event =
        await _databaseRef.child('cuentos/${widget.cuentoId}').once();
    final DataSnapshot snapshot = event.snapshot;

    setState(() {
      cuentoData = Map<String, dynamic>.from(snapshot.value as Map);
    });

    if (cuentoData!.containsKey('puntaje')) {
      dynamic puntaje = cuentoData!['puntaje'];
      if (puntaje is String) {
        setState(() {
          initialScore = int.parse(puntaje);
          totalScore = initialScore;
        });
      } else if (puntaje is int) {
        setState(() {
          initialScore = puntaje;
          totalScore = initialScore;
        });
      }
    }
  }

  Future<void> _updateScore(String subcuentoId) async {
    final DatabaseEvent event =
        await _databaseRef.child('cuentos/$subcuentoId').once();
    final DataSnapshot snapshot = event.snapshot;

    if (snapshot.value != null) {
      final Map<String, dynamic> subcuentoData =
          Map<String, dynamic>.from(snapshot.value as Map);
      if (subcuentoData.containsKey('puntaje')) {
        int puntajeToAdd = subcuentoData['puntaje'] as int;
        setState(() {
          totalScore += puntajeToAdd;
        });

        final User? user = _auth.currentUser;
        if (user != null) {
          await _databaseRef.child('users/${user.uid}').update({
            'totalScore': totalScore,
          });
        }
      }
    }
  }

  Future<void> _speak(String text) async {
    if (!isPlaying) {
      await flutterTts.setLanguage("es-ES");
      await flutterTts.setSpeechRate(0.5);
      await flutterTts.setPitch(0.1);
      text = text.replaceAll('-', '').replaceAll(r'\n', ' ');
      await flutterTts.speak(text);
      setState(() {
        isPlaying = true;
      });
    } else {
      await flutterTts.stop();
      setState(() {
        isPlaying = false;
      });
    }
  }

  @override
  void dispose() {
    flutterTts.stop();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (isPlaying) {
          await flutterTts.stop();
          setState(() {
            isPlaying = false;
          });
        }
        return true;
      },
      child: Scaffold(
        backgroundColor: const Color(0xFFE1FFD6),
        appBar: AppBar(
          backgroundColor: const Color(0xFFE1FFD6),
          title: Text(cuentoData?['titulo'] ?? 'Cargando...'),
        ),
        body: cuentoData == null
            ? Center(child: CircularProgressIndicator())
            : SingleChildScrollView(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Image.network(
                      cuentoData!['imagen'],
                      height: 200,
                      width: double.infinity,
                      fit: BoxFit.cover,
                    ),
                    SizedBox(height: 16),
                    ElevatedButton(
                      onPressed: () {
                        _speak(cuentoData!['contenido']);
                      },
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all<Color>(
                            isPlaying ? Colors.red : Colors.blue),
                        shape: MaterialStateProperty.all<OutlinedBorder>(
                            RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0))),
                        padding: MaterialStateProperty.all<EdgeInsetsGeometry>(
                            EdgeInsets.symmetric(
                                vertical: 10.0, horizontal: 20.0)),
                      ),
                      child: isPlaying
                          ? Icon(Icons.stop, color: Colors.white)
                          : Icon(Icons.play_arrow, color: Colors.white),
                    ),
                    SizedBox(height: 16),
                    Text(
                      cuentoData!['titulo_cuento'],
                      style:
                          TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(height: 16),
                    RichText(
                      text: TextSpan(
                        style: GoogleFonts.libreBaskerville(
                            fontSize: 18, color: Colors.black),
                        children: (cuentoData!['contenido'] as String)
                            .split(r'\n')
                            .map<TextSpan>((text) {
                          return TextSpan(text: text + '\n\n');
                        }).toList(),
                      ),
                    ),
                    SizedBox(height: 16),
                    if (cuentoData!.containsKey('opciones'))
                      ...cuentoData!['opciones'].entries.map<Widget>((entry) {
                        final optionData = entry.value;
                        return Container(
                          margin: EdgeInsets.symmetric(vertical: 8),
                          width: double.infinity,
                          child: ElevatedButton(
                            style: ButtonStyle(
                              backgroundColor:
                                  MaterialStateProperty.all<Color>(Colors.blue),
                              foregroundColor: MaterialStateProperty.all<Color>(
                                  Colors.white),
                            ),
                            onPressed: () {
                              if (isPlaying) {
                                flutterTts.stop();
                                setState(() {
                                  isPlaying = false;
                                });
                              }
                              _updateScore(optionData['subcuento_id']);
                              Navigator.push(
                                context,
                                PageRouteBuilder(
                                  pageBuilder:
                                      (context, animation1, animation2) {
                                    return StoryDetailPage(
                                      cuentoId: optionData['subcuento_id'],
                                    );
                                  },
                                  transitionsBuilder: (context, animation,
                                      secondaryAnimation, child) {
                                    var begin = Offset(1.0, 0.0);
                                    var end = Offset.zero;
                                    var curve = Curves.easeInOut;

                                    var tween = Tween(begin: begin, end: end)
                                        .chain(CurveTween(curve: curve));
                                    var offsetAnimation =
                                        animation.drive(tween);

                                    return SlideTransition(
                                      position: offsetAnimation,
                                      child: child,
                                    );
                                  },
                                ),
                              );
                            },
                            child: Text(optionData['texto']),
                          ),
                        );
                      }).toList(),
                    SizedBox(height: 16),
                    if (cuentoData!['final'] == true)
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          ElevatedButton.icon(
                            onPressed: () async {
                              final score = await _fetchTotalScore();
                              final reflection = await generateReflection(
                                  score, cuentoData!['contenido']);

                              showDialog(
                                context: context,
                                builder: (context) {
                                  return AlertDialog(
                                    title: Text('Reflexión'),
                                    content: Column(
                                      mainAxisSize: MainAxisSize.min,
                                      children: [
                                        SizedBox(height: 16),
                                        Text('Entonces: $reflection'),
                                        SizedBox(height: 16),
                                        ElevatedButton(
                                          onPressed: () {
                                            Navigator.of(context).popUntil(
                                                (route) => route.isFirst);
                                          },
                                          child: Text('Cerrar'),
                                        ),
                                      ],
                                    ),
                                  );
                                },
                              );
                            },
                            icon: Icon(Icons.star),
                            label: Text(
                              'Ver Resultado',
                              style: TextStyle(fontSize: 18),
                            ),
                            style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all<Color>(
                                  Colors.green),
                              foregroundColor: MaterialStateProperty.all<Color>(
                                  Colors.white),
                              shape: MaterialStateProperty.all<
                                  RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20.0),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                  ],
                ),
              ),
      ),
    );
  }

  Future<int> _fetchTotalScore() async {
    final User? user = _auth.currentUser;
    if (user != null) {
      final DatabaseEvent event =
          await _databaseRef.child('users/${user.uid}/totalScore').once();
      final DataSnapshot snapshot = event.snapshot;
      if (snapshot.value != null) {
        return snapshot.value as int;
      }
    }
    return 0;
  }

  Future<String> generateReflection(int score, String cuentoContent) async {
    String reflectionCategory;

    if (score >= 1 && score <= 5) {
      reflectionCategory = "moralidad buena";
    } else if (score >= 6 && score <= 11) {
      reflectionCategory = "moralidad neutra";
    } else if (score >= 12 && score <= 16) {
      reflectionCategory = "moralidad mala";
    } else {
      reflectionCategory = "sin categoría definida";
    }

    final response = await OpenAI.instance.completion.create(
      model: "gpt-3.5-turbo-instruct",
      prompt:
          "Genera una reflexión muy breve sobre la siguiente historia teniendo en cuenta la moralidad del jugador que se evalúa como $reflectionCategory:\n\n$cuentoContent\n\nLa reflexión debe encapsular las partes importantes del análisis del contenido del cuento en menos de 50 palabras.",
      maxTokens: 80, //Debe ser el doble de las palabras
    );
    return response.choices.first.text.trim();
  }
}
