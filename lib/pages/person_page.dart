import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:fl_chart/fl_chart.dart';

class PersonPage extends StatefulWidget {
  @override
  _PersonPageState createState() => _PersonPageState();
}

class _PersonPageState extends State<PersonPage> {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final DatabaseReference _databaseRef = FirebaseDatabase.instance.ref();
  int totalScore = 0;
  bool isLoading = true;
  bool hasError = false;

  @override
  void initState() {
    super.initState();
    _fetchTotalScore();
  }

  Future<void> _fetchTotalScore() async {
    try {
      final User? user = _auth.currentUser;
      if (user != null) {
        final DatabaseEvent event =
            await _databaseRef.child('users/${user.uid}/totalScore').once();
        final DataSnapshot snapshot = event.snapshot;

        if (snapshot.value != null) {
          setState(() {
            totalScore = snapshot.value as int;
          });
        }
      }
    } catch (e) {
      setState(() {
        hasError = true;
      });
    } finally {
      setState(() {
        isLoading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFFE1FFD6),
      appBar: AppBar(
        title: Text('Perfil de Personalidad'),
        backgroundColor: const Color(0xFFE1FFD6),
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: isLoading
              ? CircularProgressIndicator()
              : hasError
                  ? Text('Error al obtener los datos',
                      style: TextStyle(
                          color: Colors.red,
                          fontSize: 18,
                          fontWeight: FontWeight.bold))
                  : _buildPersonalityChart(),
        ),
      ),
    );
  }

  Widget _buildPersonalityChart() {
    List<PieChartSectionData> sections = _generateChartSections();

    return PieChart(
      PieChartData(
        sections: sections,
        centerSpaceRadius: 50,
        sectionsSpace: 2,
        pieTouchData: PieTouchData(
          touchCallback: (FlTouchEvent event, pieTouchResponse) {
            setState(() {});
          },
        ),
      ),
    );
  }

  List<PieChartSectionData> _generateChartSections() {
    double goodMorality =
        (totalScore >= 1 && totalScore <= 5) ? totalScore.toDouble() : 0;
    double neutralMorality =
        (totalScore >= 6 && totalScore <= 11) ? totalScore.toDouble() : 0;
    double badMorality =
        (totalScore >= 12 && totalScore <= 16) ? totalScore.toDouble() : 0;

    double undefinedMorality = (totalScore < 1 || totalScore > 16)
        ? 1.0
        : 0; // Si el puntaje no está definido

    return [
      PieChartSectionData(
        color: Colors.green,
        value: goodMorality,
        title: 'Buena',
        radius: 100,
        titleStyle: TextStyle(
            fontSize: 16, fontWeight: FontWeight.bold, color: Colors.white),
      ),
      PieChartSectionData(
        color: Colors.yellow,
        value: neutralMorality,
        title: 'Neutra',
        radius: 100,
        titleStyle: TextStyle(
            fontSize: 16, fontWeight: FontWeight.bold, color: Colors.white),
      ),
      PieChartSectionData(
        color: Colors.red,
        value: badMorality,
        title: 'Mala',
        radius: 100,
        titleStyle: TextStyle(
            fontSize: 16, fontWeight: FontWeight.bold, color: Colors.white),
      ),
      if (undefinedMorality > 0)
        PieChartSectionData(
          color: Colors.grey,
          value: undefinedMorality,
          title: 'No definida',
          radius: 100,
          titleStyle: TextStyle(
              fontSize: 16, fontWeight: FontWeight.bold, color: Colors.white),
        ),
    ];
  }
}
