import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:firebase_database/firebase_database.dart';
import 'story_detail_page.dart';
import 'login_page.dart';
import 'person_page.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final ValueNotifier<bool> _isLoggedIn = ValueNotifier<bool>(false);
  User? _user;
  late AudioPlayer _audioPlayer;
  bool _isPlaying = false;
  late DatabaseReference _musicStateRef;

  @override
  void initState() {
    super.initState();
    _audioPlayer = AudioPlayer();
    _musicStateRef = FirebaseDatabase.instance.reference().child('users');
    _initializeMusicState();
  }

  Future<void> _initializeMusicState() async {
    User? user = FirebaseAuth.instance.currentUser;
    if (user != null) {
      _user = user;
      if (mounted) {
        _isLoggedIn.value = true;
      }
      await _checkMusicState();
    } else {
      if (mounted) {
        await _playMusic();
      }
    }
  }

  Future<void> _playMusic() async {
    await _audioPlayer.setReleaseMode(ReleaseMode.loop);
    await _audioPlayer.play(AssetSource('music/musica.ogg'));
    _updateIsPlayingState(true);
  }

  Future<void> _toggleMusic() async {
    bool newPlayingState = !_isPlaying;

    if (newPlayingState) {
      await Future.delayed(Duration(seconds: 2));
      await _audioPlayer.resume();
    } else {
      await _audioPlayer.pause();
    }

    _updateIsPlayingState(newPlayingState);

    if (_user != null) {
      await _musicStateRef
          .child(_user!.uid)
          .set({'isPlaying': newPlayingState});
    }
  }

  Future<void> _checkMusicState() async {
    if (_user != null) {
      _musicStateRef.child(_user!.uid).onValue.listen((event) {
        if (!mounted) return;

        DataSnapshot dataSnapshot = event.snapshot;
        var value = dataSnapshot.value;

        if (value != null && value is Map<dynamic, dynamic>) {
          bool shouldPlay = value['isPlaying'] ?? false;

          if (shouldPlay) {
            Future.delayed(Duration(seconds: 2), () {
              if (mounted) {
                _playMusic();
              }
            });
          } else {
            if (mounted) {
              _updateIsPlayingState(false);
              _audioPlayer.pause();
            }
          }
        }
      });
    }
  }

  void _updateIsPlayingState(bool newState) {
    if (mounted) {
      setState(() {
        _isPlaying = newState;
      });
    }
  }

  @override
  void dispose() {
    _audioPlayer.dispose();
    _isLoggedIn.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFFE1FFD6),
      body: PageView(
        children: [
          _buildHomePage(context), // Página principal
          PersonPage(), // Página deslizable
        ],
      ),
    );
  }

  Widget _buildHomePage(BuildContext context) {
    return SingleChildScrollView(
      child: Stack(
        children: [
          Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 10, top: 20),
                      child: ValueListenableBuilder<bool>(
                        valueListenable: _isLoggedIn,
                        builder: (context, isLoggedIn, child) {
                          return SizedBox(
                            width: 50,
                            height: 50,
                            child: IconButton(
                              icon: isLoggedIn
                                  ? const Icon(Icons.logout, size: 30)
                                  : Image.asset(
                                      'assets/imagenes/google_login.jpg'),
                              iconSize: 30,
                              onPressed:
                                  isLoggedIn ? _signOut : _signInWithGoogle,
                            ),
                          );
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right: 10, top: 20),
                      child: SizedBox(
                        width: 50,
                        height: 50,
                        child: IconButton(
                          icon: Icon(
                            _isPlaying ? Icons.pause : Icons.music_note,
                            size: 30,
                          ),
                          onPressed: _toggleMusic,
                        ),
                      ),
                    ),
                  ],
                ),
                Image.asset(
                  'assets/imagenes/134187.jpg',
                  width: 160,
                  height: 160,
                ),
                const SizedBox(height: 10),
                const SizedBox(
                  width: 210,
                  height: 45,
                  child: Center(
                    child: Text(
                      '¡Diviértete!',
                      style: TextStyle(
                        fontSize: 32,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'KronaOne',
                      ),
                    ),
                  ),
                ),
                const SizedBox(height: 20),
                Padding(
                  padding: const EdgeInsets.only(right: 120),
                  child: CustomButton(
                    icon: Icons.rocket,
                    text: 'Cuento 1',
                    onPressed: () {
                      _navigateToStoryDetailPage(context, 'C01');
                    },
                  ),
                ),
                const SizedBox(height: 20),
                Padding(
                  padding: const EdgeInsets.only(left: 140),
                  child: CustomButton(
                    icon: Icons.rocket,
                    text: 'Cuento 2',
                    onPressed: () {
                      _navigateToStoryDetailPage(context, 'C02');
                    },
                  ),
                ),
                const SizedBox(height: 20),
                Padding(
                  padding: const EdgeInsets.only(right: 120),
                  child: CustomButton(
                    icon: Icons.rocket,
                    text: 'Cuento 3',
                    onPressed: () {
                      _navigateToStoryDetailPage(context, 'C03');
                    },
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Future<void> _signInWithGoogle() async {
    try {
      final GoogleSignInAccount? googleUser = await GoogleSignIn().signIn();
      if (googleUser != null) {
        final GoogleSignInAuthentication googleAuth =
            await googleUser.authentication;
        final AuthCredential credential = GoogleAuthProvider.credential(
          accessToken: googleAuth.accessToken,
          idToken: googleAuth.idToken,
        );
        UserCredential userCredential =
            await FirebaseAuth.instance.signInWithCredential(credential);
        _user = userCredential.user;

        if (_user != null && mounted) {
          _isLoggedIn.value = true;
          await _checkMusicState();
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text(
                "Inicio de sesión exitoso, ¡Bienvenido ${_user?.displayName ?? 'Usuario'}!"),
          ));
        }
      }
    } catch (error) {
      print("Error de inicio de sesión con Google: $error");
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Error de inicio de sesión con Google: $error"),
      ));
    }
  }

  Future<void> _signOut() async {
    try {
      if (_isPlaying) {
        await _audioPlayer.stop();
        _updateIsPlayingState(false);
      }

      await FirebaseAuth.instance.signOut();
      await GoogleSignIn().signOut();

      if (_user != null) {
        await _musicStateRef.child(_user!.uid).set({'isPlaying': false});
      }

      if (mounted) {
        _isLoggedIn.value = false;
        _user = null;

        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: const Text("Cierre de sesión exitoso."),
        ));

        Navigator.pushReplacement(
          context,
          MaterialPageRoute(builder: (context) => const LoginPage()),
        );
      }
    } catch (error) {
      print("Error al cerrar sesión: $error");
      if (mounted) {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text("Error al cerrar sesión: $error"),
        ));
      }
    }
  }

  void _navigateToStoryDetailPage(BuildContext context, String cuentoId) {
    Navigator.push(
      context,
      PageRouteBuilder(
        pageBuilder: (context, animation1, animation2) =>
            StoryDetailPage(cuentoId: cuentoId),
        transitionsBuilder: (context, animation1, animation2, child) {
          return FadeTransition(
            opacity: animation1,
            child: child,
          );
        },
        transitionDuration: const Duration(milliseconds: 500),
      ),
    );
  }
}

class CustomButton extends StatelessWidget {
  final IconData icon;
  final String text;
  final VoidCallback? onPressed;

  const CustomButton({
    Key? key,
    required this.icon,
    required this.text,
    this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 130,
      height: 130,
      child: ElevatedButton(
        onPressed: onPressed,
        style: ButtonStyle(
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
            RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15.0),
            ),
          ),
          backgroundColor: MaterialStateProperty.all<Color>(
            const Color.fromARGB(174, 73, 147, 221),
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 11),
              child: Transform.rotate(
                angle: 45 * 3.14159 / 180,
                child: Icon(
                  icon,
                  size: 60,
                  color: Colors.white,
                ),
              ),
            ),
            const SizedBox(height: 8),
            Center(
              child: Text(
                text,
                style: const TextStyle(
                  fontSize: 16,
                  color: Colors.white,
                  fontFamily: 'KronaOne',
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
